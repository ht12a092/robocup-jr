Githubへ移行しました．  
https://github.com/ht12a092/SCORER

# RoboCup Jr. SCORER

## 必要な開発環境
* node v0.12 または 互換性のあるバージョン

## 開発の始め方

以下のコマンドによりリポジトリをお使いのコンピュータにクローンし、依存モジュールをインストールします。

    $ git clone git@bitbucket.org:ht12a092/robocup-jr.git
    $ cd robocup-jr/
    $ npm install

以下のコマンドにより開発用サーバを起動することができます。

    $ npm start

通常、サーバは http://127.0.0.1:3000/ からアクセスできます。

Enjoy :)